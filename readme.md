# Php List for mailing

Phplist es una librería de PHP para el envío de campañas de email.

## Instalación

Clonar el repositorio en la raíz del sitio. [PHPList](https://gitlab.com/4to-pixel/lists)

```bash
git clone https://gitlab.com/4to-pixel/lists
```

## Configuración
En el archivo config/config.php establecer los datos de la conexión, prefijo de las tablas y todas las opciones de personalización.
[Ver documentación](https://www.phplist.org/manual/books/phplist-manual/)

## Sendgrid
En el archivo config/config.php, a partir de la línea 584 se encuentran los datos de conexión de [Sendgrid](https://sendgrid.com/)

## Plugins
En la url lists/admin/?page=plugins se pueden gestionar los plugins
<img src="https://gitlab.com/4to-pixel/lists/-/raw/0795b3d5c0262a5119193de46b94677edba0047a/uploadimages/manage-plugins.png"/>
### Common Plugin
Installation url: https://github.com/bramley/phplist-plugin-common/archive/master.zip

### SendGrid Plugin
Installation url: https://github.com/bramley/phplist-plugin-sendgrid/archive/master.zip

### Simple attribute select
Installation url: https://github.com/michield/phplist-plugin-attributeselect/archive/master.zip

## License
[MIT](https://choosealicense.com/licenses/mit/)